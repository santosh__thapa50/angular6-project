import {Component, OnInit} from '@angular/core';
import {UiService} from '../../../_services';

@Component({
  selector: 'app-weather-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class WeatherHomeComponent implements OnInit {

   showMenu = false;
  darkModeActive: boolean;

  constructor(public ui: UiService) {

  }

  ngOnInit() {
    this.ui.darkModeState.subscribe((value) => {
      this.darkModeActive = value;
    });
  }

  toggleMenu() {
    this.showMenu = !this.showMenu;
  }

  modeToggleSwitch() {
    this.ui.darkModeState.next(!this.darkModeActive);
  }
}
