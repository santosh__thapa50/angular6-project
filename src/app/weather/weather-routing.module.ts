import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WeatherHomeComponent} from './pages/home/home.component';
import {DetailsComponent} from './pages/details/details.component';
import {AddComponent} from './pages/add/add.component';
//import {LoginComponent} from './pages/login/login.component';
//import {SignupComponent} from './pages/signup/signup.component';

const routes: Routes = [
  {path: 'weather', component: WeatherHomeComponent},
  {path: 'details/:city', component: DetailsComponent},
  {path: 'add', component: AddComponent},
  //{path: 'login', component: LoginComponent},
  //{path: 'signup', component: SignupComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class WeatherRoutingModule {
}
