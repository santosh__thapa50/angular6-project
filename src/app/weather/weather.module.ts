import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {WeatherRoutingModule} from './weather-routing.module';
import {WeatherComponent} from './weather.component';

import {WeatherHomeComponent} from './pages/home/home.component';
import {DetailsComponent} from './pages/details/details.component';
import {WeatherService} from '../_services';
import {HttpClientModule} from '@angular/common/http';
import {WeatherCardComponent} from './ui/weather-card/weather-card.component';
import {AddCardComponent} from './ui/add-card/add-card.component';
import {AddComponent} from './pages/add/add.component';
import {LoginComponent} from './pages/login/login.component';
import {SignupComponent} from './pages/signup/signup.component';
import {UiService} from '../_services';

@NgModule({
  declarations: [
    WeatherComponent,
    WeatherHomeComponent,
    DetailsComponent,
    WeatherCardComponent,
    AddCardComponent,
    AddComponent,
    LoginComponent,
    SignupComponent,
    AddCardComponent
  ],
  imports: [
    BrowserModule,
    WeatherRoutingModule,
    HttpClientModule,
  ],
  exports:[
  WeatherRoutingModule,
  WeatherComponent
  ],
  providers: [
    WeatherService,
    UiService
  ],
  bootstrap: [WeatherComponent]
})
export class WeatherModule {
}
